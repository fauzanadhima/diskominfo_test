import axios from 'axios';
import {BASE_URL} from '../utils/config';

export const apiGetListNews = page => {
  return axios(BASE_URL + '/news?page=' + page)
    .then(res => res)
    .catch(err => err);
};

export const apiGetDetailNews = newsId => {
  return axios(BASE_URL + '/news/' + newsId)
    .then(res => res)
    .catch(err => err);
};
