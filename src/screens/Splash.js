import React, {useEffect, useState} from 'react';
import {View, StyleSheet, Animated, Image} from 'react-native';
import {images} from '../assets';

const Splash = ({navigation}) => {
  const [textShow] = useState(new Animated.Value(20));

  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('Home');
    }, 5000);
  }, [navigation, textShow]);

  return (
    <View style={styles.container}>
      <Animated.View
        // eslint-disable-next-line react-native/no-inline-styles
        style={{
          position: 'absolute',
        }}>
        <Image style={styles.logo} source={images.logo} />
      </Animated.View>
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: 100,
    height: 100,
  },
});
