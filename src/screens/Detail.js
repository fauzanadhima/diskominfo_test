import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  ImageBackground,
  Dimensions,
} from 'react-native';
import Header from '../components/Header';
import moment from 'moment';
import RenderHTML from 'react-native-render-html';

const width = Dimensions.get('window').width;

const height = Dimensions.get('window').height;

const tagsStyles = {
  p: {
    fontWeight: '400',
    color: '#000000',
    fontSize: width * 0.04,
    lineHeight: 25,
  },
  div: {
    textAlign: 'justify',
    color: '#000000',
  },
};

const Detail = ({navigation, route}) => {
  // props or params
  let routeParams = route?.params.params.data;

  return (
    <View style={styles.container}>
      <Header headerTitle={'Detail News'} onBack={() => navigation.goBack()} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <ImageBackground source={routeParams.image} style={styles.image}>
          <View style={styles.background}>
            <View style={styles.backgroundText}>
              <Text style={styles.datetext}>
                {moment(routeParams?.created_at).format('dddd, DD MMMM YYYY')}
              </Text>
              <Text style={styles.titleText} numberOfLines={3}>
                {routeParams?.judul_posting}
              </Text>
              <Text style={styles.publishText}>Published by Diskominfo</Text>
            </View>
          </View>
        </ImageBackground>
        <View style={styles.containerContent} stylesheet={styles}>
          <RenderHTML
            contentWidth={width}
            tagsStyles={tagsStyles}
            source={{html: routeParams?.isi_posting}}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default Detail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  newsImage: {
    width: '100%',
    height: 400,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    padding: 10,
  },
  newsTitle: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  textTitleCenter: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
  image: {
    width: width * 1,
    height: height * 0.45,
  },
  datetext: {
    fontSize: width * 0.03,
    fontWeight: '400',
    color: '#2E0505',
    fontStyle: 'normal',
  },
  background: {
    marginTop: height * 0.25,
    marginHorizontal: width * 0.05,
    marginBottom: height * 0.06,
    backgroundColor: 'rgba(245, 245, 245, 0.8)',
    width: width * 0.9,
    // height: height * 0.18,
    borderRadius: width * 0.04,
  },
  backgroundText: {
    margin: width * 0.05,
  },
  titleText: {
    fontSize: width * 0.04,
    fontWeight: '900',
    color: '#2E0505',
    marginVertical: height * 0.007,
  },
  publishText: {
    fontSize: width * 0.03,
    fontWeight: '400',
    color: '#2E0505',
    // marginVertical: height * 0.02,
  },
  containerContent: {
    marginHorizontal: width * 0.05,
    marginVertical: height * 0.01,
  },
});
