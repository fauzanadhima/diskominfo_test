const images = {
  logo: require('./images/Logo_Kementerian_Kominfo.png'),
  foto_1: require('./images/image_1.jpg'),
  foto_2: require('./images/image_2.jpg'),
  foto_3: require('./images/image_3.jpg'),
  foto_4: require('./images/image_4.jpg'),
  foto_5: require('./images/image_5.jpg'),
};

export default images;
