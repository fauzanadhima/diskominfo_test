import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Image,
} from 'react-native';
import Header from '../components/Header';
import {images} from '../assets';
import moment from 'moment';

const Home = ({navigation, route}) => {
  const data = {
    data: [
      {
        id_posting: 1044510,
        posisi: 'highlight',
        judul_posting:
          'Agar Go Internasional, Bupati Minta Pelaku UMKM Update dan Upgrade Produknya',
        slug: 'agar-go-internasional-bupati-minta-pelaku-umkm-update-dan-upgrade-produknya',
        isi_posting:
          '<p style="text-align: justify;">Melalui strategi update dan upgrade produk, diharapkan pelaku Usaha Mikro Kecil dan Menengah (UMKM) mampu meningkatkan kualitas produknya. Sehingga akan berdampak perluasan pangsa pasar bahkan diharapkan mampu menembus pangsa pasar internasional.</p>\r\n\r\n<p style="text-align: justify;">Hal tersebut disampaikan Bupati Wonosobo Afif Nurhidayat dalam arahannya saat Launching Kemitraan UMKM dengan PT Indomarco Prismatama, Rabu, (25/01/2023), di Pendopo Selatan.</p>\r\n\r\n<p style="text-align: justify;">Menurut Afif, UMKM di Kabupaten Wonosobo memiliki peluang yang besar untuk naik kelas dan go international, baik yang dijalankan perorangan maupun perusahaan. Hal tersebut, jika dikelola dengan sungguh-sungguh dan profesional.</p>\r\n\r\n<p style="text-align: justify;">&ldquo;Produk UMKM Wonosobo yang dikelola perorangan maupun perusahaan sudah ada yang diekspor ke luar negeri, itu menandakan bahwa produk UMKM kita banyak diminati oleh warga internasional,&rdquo; ungkap Afif.</p>\r\n\r\n<p style="text-align: justify;">Diharapkan, para pelaku UMKM Wonosobo lebih peka menatap segala nilai kebaruan dalam berusaha, baik menjaga kontinuitas produk maupun kemasan dan kwalitas produknya. Disamping itu, juga menjaga kepercayaan yang diberikan pelanggan.&nbsp;</p>\r\n\r\n<p style="text-align: justify;">&ldquo;Pemkab memberikan ruang seluas-luasnya bagi seluruh pelaku UMKM bagaimana berkompetisi yang sehat dan dinamis, ciptakan ide-ide kreatif masing-masing guna keberlangsungan usaha,&rdquo; tandasnya</p>\r\n\r\n<p style="text-align: justify;">Sementara itu, Kepala Bidang Perdagangan Dinas Perdagangan, Koperasi, Usaha Kecil Menengah (UKM) Kabupaten Wonosobo Eni menyampaikan, PT Indomarco Prismatama merupakan toko ritel yang setiap tahun melakukan tanggung jawab sosial dan kewajibannya dalam kegiatan kurasi produk UMKM di Wonosobo. Terhitung sampai saat ini ada 29 produk UMKM Wonosobo yang berhasil masuk ke toko modern dan menjadi mitra Indomaret.</p>\r\n\r\n<p style="text-align: justify;">&ldquo;Saya berharap, setelah mengikuti kegiatan ini peserta pelaku UMKM Wonosobo dapat menerapkan dan memberdayakan produk UMKM-nya, meningkatkan penjualan produk UMKM melalui pangsa pasar Indomaret, dapat meningkatkan pengetahuan tentang bagaimana ketentuan hasil produk dapat masuk toko modern, serta menjalin sinergitas antara pelaku UMKM dengan PT Indomarco Prismatama dan Pemkab Wonosobo,&rdquo; imbuh Eni dihadapan 75 peserta yang hadir.</p>\r\n\r\n<p style="text-align: justify;">Perwakilan PT Indomarco Prismatama Cabang Yogyakarta Tafan Russuardi menambahkan, pelaku UMKM di Wonosobo dapat terus berkreasi membuka potensi ruang usaha yang lebih strategis melalui pangsa pasar modern.</p>\r\n\r\n<p style="text-align: justify;">&ldquo;Dengan diresmikannya 29 produk UMKM ini, semoga ke depan kurasi produknya bertambah lagi, kami memiliki 26 mitra UMKM dengan produk jajanan nusantara dan kue basah, batik nusantara khas daerah, komoditas pertanian, dan sebagainya,&rdquo; tandasnya.</p>\r\n\r\n<p style="text-align: justify;">Pada kegiatan tersebut, peserta selain diberikan pelatihan kewirausahaan mandiri juga mendapatkan timbangan digital 75 unit untuk tingkatkan usahanya.</p>',
        kata_kunci:
          'Agar Go Internasional, Bupati Minta Pelaku UMKM Update dan Upgrade Produknya',
        id_kategori: '1',
        informasi_st: 'INFORMASI_ST_02',
        temp: null,
        created_by: 1008,
        created_at: '2023-01-26T00:55:56.000000Z',
        updated_at: '2023-01-26T04:05:54.000000Z',
        updated_by: null,
        deleted_by: null,
        deleted_at: null,
        keterangan:
          'Melalui strategi update dan upgrade produk, diharapkan pelaku Usaha Mikro Kecil dan Menengah (UMKM) mampu meningkatkan kualitas produknya. Sehingga akan...',
        views: 4,
        image: images.foto_1,
        attachment: [
          {
            id_attachment: 24128,
            id_tabel: 1044510,
            path: 'uploads/2023/Januari/',
            file_name: '20230126075556-1_1008.jpg',
            temp: null,
            created_at: '2023-01-26T00:55:56.000000Z',
            updated_at: '2023-01-26T00:55:56.000000Z',
            deleted_at: null,
          },
          {
            id_attachment: 24129,
            id_tabel: 1044510,
            path: 'uploads/2023/Januari/',
            file_name: '20230126075556-2_1008.jpg',
            temp: null,
            created_at: '2023-01-26T00:55:56.000000Z',
            updated_at: '2023-01-26T00:55:56.000000Z',
            deleted_at: null,
          },
          {
            id_attachment: 24130,
            id_tabel: 1044510,
            path: 'uploads/2023/Januari/',
            file_name: '20230126075556-3_1008.jpg',
            temp: null,
            created_at: '2023-01-26T00:55:56.000000Z',
            updated_at: '2023-01-26T00:55:56.000000Z',
            deleted_at: null,
          },
        ],
        gambar_muka: {
          id_attachment: 24128,
          id_tabel: 1044510,
          path: 'uploads/2023/Januari/',
          file_name: '20230126075556-1_1008.jpg',
          temp: null,
          created_at: '2023-01-26T00:55:56.000000Z',
          updated_at: '2023-01-26T00:55:56.000000Z',
          deleted_at: null,
        },
      },
      {
        id_posting: 1044509,
        posisi: 'highlight',
        judul_posting: 'Bawa Aspirasi, 850 Perangkat Desa Bertolak ke Jakarta',
        slug: 'bawa-aspirasi-850-perangkat-desa-bertolak-ke-jakarta',
        isi_posting:
          '<p style="text-align: justify;">Sebanyak 850 perangkat desa dari Kabupaten Wonosobo yang tergabung dalam Persatuan Perangkat Desa Indonesia (PPDI) Kabupaten Wonosobo bertolak menuju Jakarta, Selasa (24/1/2022)&nbsp; guna mengikuti Silaturahmi&nbsp;Nasional PPDI Jilid III di Jakarta.</p>\r\n\r\n<p style="text-align: justify;">Rombongan yang merupakan perwakilan dari 263 desa di Wonosobo tersebut berangkat menggunakan 17 bus untuk bergabung dengan perangkat desa dari seluruh Indonesia guna menyampaikan aspirasinya.</p>\r\n\r\n<p style="text-align: justify;">Ketua PPDI kabupaten Wonosobo Darjin Traju Visa menjelaskan, keberangkatan hampir seperempat dari perangkat desa di Wonosobo&nbsp; tersebut membawa aspirasi seluruh perangkat yang ada, dengan&nbsp;beberapa tuntutan antara lain, menolak gagasan beberapa pihak yang menghendaki periodesasi perangkat desa sama dengan kepala desa, juga menolak wacana status kepegawaian perangkat desa menjadi PNS atau P3K.</p>\r\n\r\n<p style="text-align: justify;">Tambah Darjin, utusan perangkat desa dari Wonosobo akan fokus menyuarakan kejelasan status kepegawaian perangkat desa, dengan memperhatikan masa kerja serta memberikan NIAPD. Juga terkait kepastian perlindungan hukum terhadap perangkat desa, yang memberikan rasa aman dan nyaman dalam bekerja, sehingga dapat melaksanakan pembangunan di desa dan pengabdian terhadap masyarakat secara total.</p>\r\n\r\n<p style="text-align: justify;">&rdquo;Nomor Induk Perangkat Desa (NIPD) penting adanya untuk menguatkan kedudukan perangkat desa sebagai bagian dari penyelenggara pemerintahan, terutama dalam tata kelola administrasi pemerintahan desa, masa jabatan perangkat desa tetap maksimal 60 tahun sebagaimana Undang Undang Nomor 6 Tahun 2014 Tentang Desa,&rdquo; jelasnya.</p>\r\n\r\n<p style="text-align: justify;">Jika aspirasi terpenuhi, Darjin memastikan, akan berdampak terhadap kinerja dalam melayani dan mengabdi kepada masyarakat di masing-masing desa. Pasalnya, perangkat akan bekerja dengan lebih aman, nyaman dan total melayani masyarakat 24 jam.</p>\r\n\r\n<p style="text-align: justify;">&ldquo;Kami mendorong penuh&nbsp;Pemerintah&nbsp;Pusat untuk bisa menerbitkan suatu aturan tentang keperangkatan desa yang lebih konkret&nbsp;dan jelas, di undang-undang yang berlaku saat ini baik &nbsp;Perpres maupun&nbsp;Kemendagri belum secara rigid mengatur tentang perangkat desa,&rdquo; tambah Darjin.</p>\r\n\r\n<p style="text-align: justify;">Sementara itu Bupati Wonosobo Afif Nurhidayat kepada media menyampaikan, Silatnas PPID Jilid III diselenggarakan tentu dengan beberapa agenda yang sudah tersusun dan terjadwalkan, harapannya semua terlaksana dengan sukses dan terpenuhi aspirasinya, baik penguatan kelembagaan maupun peningkatan&nbsp; kesejahteraan.</p>\r\n\r\n<p style="text-align: justify;">&ldquo;Kami mendukung penuh ikhtiar PPDI Wonosobo dalam membawa aspirasinya untuk disampaikan dalam forum Silatnas, semoga tercapai apa yang menjadi tujuan dan maksud diadakannya Silatnas,&rdquo; tandasnya</p>',
        kata_kunci: 'Bawa Aspirasi, 850 Perangkat Desa Bertolak ke Jakarta',
        id_kategori: '1',
        informasi_st: 'INFORMASI_ST_02',
        temp: null,
        created_by: 1008,
        created_at: '2023-01-25T07:05:04.000000Z',
        updated_at: '2023-01-25T10:36:29.000000Z',
        updated_by: null,
        deleted_by: null,
        deleted_at: null,
        keterangan:
          'Sebanyak 850 perangkat desa dari Kabupaten Wonosobo yang tergabung dalam Persatuan Perangkat Desa Indonesia (PPDI) Kabupaten Wonosobo bertolak menuju...',
        views: 2,
        image: images.foto_2,
        attachment: [
          {
            id_attachment: 24124,
            id_tabel: 1044509,
            path: 'uploads/2023/Januari/',
            file_name: '20230125020504-1_1008.jpg',
            temp: null,
            created_at: '2023-01-25T07:05:04.000000Z',
            updated_at: '2023-01-25T07:05:04.000000Z',
            deleted_at: null,
          },
          {
            id_attachment: 24125,
            id_tabel: 1044509,
            path: 'uploads/2023/Januari/',
            file_name: '20230125020504-2_1008.jpg',
            temp: null,
            created_at: '2023-01-25T07:05:04.000000Z',
            updated_at: '2023-01-25T07:05:04.000000Z',
            deleted_at: null,
          },
          {
            id_attachment: 24126,
            id_tabel: 1044509,
            path: 'uploads/2023/Januari/',
            file_name: '20230125020505-3_1008.jpg',
            temp: null,
            created_at: '2023-01-25T07:05:05.000000Z',
            updated_at: '2023-01-25T07:05:05.000000Z',
            deleted_at: null,
          },
          {
            id_attachment: 24127,
            id_tabel: 1044509,
            path: 'uploads/2023/Januari/',
            file_name: '20230125020505-4_1008.jpg',
            temp: null,
            created_at: '2023-01-25T07:05:05.000000Z',
            updated_at: '2023-01-25T07:05:05.000000Z',
            deleted_at: null,
          },
        ],
        gambar_muka: {
          id_attachment: 24124,
          id_tabel: 1044509,
          path: 'uploads/2023/Januari/',
          file_name: '20230125020504-1_1008.jpg',
          temp: null,
          created_at: '2023-01-25T07:05:04.000000Z',
          updated_at: '2023-01-25T07:05:04.000000Z',
          deleted_at: null,
        },
      },
      {
        id_posting: 1044508,
        posisi: 'highlight',
        judul_posting: 'KPU Wonosobo Lantik 795 Anggota PPS Pemilu 2024',
        slug: 'kpu-wonosobo-lantik-795-anggota-pps-pemilu-2024',
        isi_posting:
          '<p style="text-align: justify;">Guna menyukseskan kontestasi pentas demokrasi Pemilu Serentak 2024 mendatang, Komisi Pemilihan Umum (KPU) Kabupaten Wonosobo resmi melantik 795 Anggota Panitia Pemungutan Suara (PPS), Selasa, (24/01/2023), di Gedung Sasana Adipura Kencana.&nbsp;</p>\r\n\r\n<p style="text-align: justify;">&ldquo;Anggota PPS yang dilantik terdiri atas 3 orang dari setiap desa se-Kabupaten Wonosobo,&rdquo; jelas Ketua KPU Kabupaten Wonosobo Asma Khozin saat diwawancarai awak media usai acara pelantikan dan pembekalan PPS Pemilu 2024 KPU Kabupaten Wonosobo.</p>\r\n\r\n<p style="text-align: justify;">Jelas Khozin, anggota PPS dipersiapkan untuk menyukseskan penyelenggaraan Pemilu 2024, mulai dari proses sosialisasi, penugasan teknis, hingga evaluasi pelaksanaan pesta demokrasi di tingkat desa.&nbsp;</p>\r\n\r\n<p style="text-align: justify;">&ldquo;Selama proses rekrutmen PPS ini masih terdapat beberapa kendala antara lain banyak individu yang kesusahan menggunakan aplikasi SIAKBA&nbsp; atau Sistem Informasi Anggota KPU dan Badan Ad Hoc, dari 7000 orang yang berhasil mendapatkan akses diambil 30% atau 795 orang,&rdquo; jelasnya.</p>\r\n\r\n<p style="text-align: justify;">Setelah acara pelantikan ini, ujar Asma,&nbsp; dilanjutkan pembentukan pantarlih pada 26 Januari mendatang,&nbsp; dan&nbsp; akan melakukan verifikasi faktual 8 calon anggota DPD Wonosobo pada bulan berikutnya.</p>\r\n\r\n<p style="text-align: justify;">&ldquo;Masa kerja PPS yang sudah dilantik terhitung mulai hari ini sampai 16 bulan ke depan, namun ada kemungkinan diperpanjang sampai kontestasi Pilkada selesai, tahun ini juga terdapat peningkatan jumlah TPS dibanding Pemilu 2019, dari 2950 TPS menjadi 3160 TPS,&rdquo; tambahnya.&nbsp;</p>\r\n\r\n<p style="text-align: justify;">Pada saat bertugas nanti, pihaknya akan memberikan stimulan berupa vitamin dan sejenisnya agar mampu bekerja maksimal di tempat tugas masing-masing.</p>\r\n\r\n<p style="text-align: justify;">&ldquo;Saya berharap, Anggota PPS Wonosobo mampu bekerja penuh dengan integritas, profesional melayani peserta Pemilu dan pemilih, serta benar-benar bekerja sesuai dengan regulasi yang kita miliki,&rdquo; pungkasnya.</p>',
        kata_kunci: 'KPU Wonosobo Lantik 795 Anggota PPS Pemilu 2024',
        id_kategori: '1',
        informasi_st: 'INFORMASI_ST_02',
        temp: null,
        created_by: 1008,
        created_at: '2023-01-25T01:25:52.000000Z',
        updated_at: '2023-01-26T03:30:31.000000Z',
        updated_by: null,
        deleted_by: null,
        deleted_at: null,
        keterangan:
          'Guna menyukseskan kontestasi pentas demokrasi Pemilu Serentak 2024 mendatang, Komisi Pemilihan Umum (KPU) Kabupaten Wonosobo resmi melantik 795 Anggota...',
        views: 9,
        image: images.foto_3,
        attachment: [
          {
            id_attachment: 24121,
            id_tabel: 1044508,
            path: 'uploads/2023/Januari/',
            file_name: '20230125082552-1_1008.jpg',
            temp: null,
            created_at: '2023-01-25T01:25:52.000000Z',
            updated_at: '2023-01-25T01:25:52.000000Z',
            deleted_at: null,
          },
          {
            id_attachment: 24122,
            id_tabel: 1044508,
            path: 'uploads/2023/Januari/',
            file_name: '20230125082552-2_1008.jpg',
            temp: null,
            created_at: '2023-01-25T01:25:52.000000Z',
            updated_at: '2023-01-25T01:25:52.000000Z',
            deleted_at: null,
          },
          {
            id_attachment: 24123,
            id_tabel: 1044508,
            path: 'uploads/2023/Januari/',
            file_name: '20230125082552-3_1008.jpg',
            temp: null,
            created_at: '2023-01-25T01:25:52.000000Z',
            updated_at: '2023-01-25T01:25:52.000000Z',
            deleted_at: null,
          },
        ],
        gambar_muka: {
          id_attachment: 24121,
          id_tabel: 1044508,
          path: 'uploads/2023/Januari/',
          file_name: '20230125082552-1_1008.jpg',
          temp: null,
          created_at: '2023-01-25T01:25:52.000000Z',
          updated_at: '2023-01-25T01:25:52.000000Z',
          deleted_at: null,
        },
      },
      {
        id_posting: 1044507,
        posisi: 'highlight',
        judul_posting:
          'Realisasi Belanja Proyek Strategis  Pemkab Wonosobo  Tahun 2022 Capai 89,60%',
        slug: 'realisasi-belanja-proyek-strategis-pemkab-wonosobo-tahun-2022-capai-89-60',
        isi_posting:
          '<p style="text-align: justify;">Realisasi belanja Pemerintah Daerah Kabupaten Wonosobo di tahun 2022, capai Rp 1,9 triliun dari pagu anggaran Rp 2,1 triliun atau 89,60%, dalam bentuk belanja modal maupun belanja non modal.</p>\r\n\r\n<p style="text-align: justify;">Hal tersebut, disampaikan Kepala Bagian Administrasi Sekretariat Daerah Wonosobo Zulfa Akhsan Alim Kurniawan dalam sambutannya saat acara Peresmian Hasil Kegiatan Pembangunan Tahun Anggaran 2022 Kabupaten Wonosobo, Selasa, (24/01/2023), di Alun-Alun Sapuran.</p>\r\n\r\n<p style="text-align: justify;">Jelas Zulfa, anggaran belanja tersebut bersumber dari pendapatan asli daerah (PAD), Dana Alokasi Umum (DAU), Dana Alokasi Khusus (DAK), dan Bantuan Keuangan Provinsi Jawa Tengah (Bankeu). Selain itu, juga didapatkan dari Dana Insentif Daerah (DID), Dana Bagi Hasil Cukai Hasil Tembakau (DBHCHT), Pajak Rokok, dan Program Hibah Jalan Daerah (PHJD).</p>\r\n\r\n<p style="text-align: justify;">&ldquo;Belanja modal gedung dan bangunan terealisasi Rp 71.528.271.760,00 dari pagu anggaran Rp 90.864.805.943,00 atau 78,72%, sedangkan untuk belanja modal sektor jalan, jembatan, jaringan, dan irigasi terealisasi Rp 210.308.031.946,00 dari pagu anggaran Rp 214.584.787.070,00 atau sebesar 98,01%,&rdquo; tambahnya.</p>\r\n\r\n<p style="text-align: justify;">Diharapkan, dari kegiatan pembangunan yang telah dilaksanakan menjadi pengungkit kesejahteraan masyarakat, meningkatkan daya saing dan memajukan Wonosobo. Antara lain,&nbsp; revitalisasi Gelanggang Olahraga Mangli di Kelurahan Kejiwan, pembangunan venue GOR Wonolelo, pembangunan Pasar Sapuran, pembangunan Puskesmas Kejajar II, pembangunan Jembatan Keseneng di ruas 118 Keseneng-Candiyasan, dan peningkatan kapasitas struktur jalan Kalibeber-Depok Mojotengah.&nbsp;</p>\r\n\r\n<p style="text-align: justify;">Termasuk kegiatan, peningkatan kapasitas struktur jalan Rake Pikatan (Kejajar-Wates), peningkatan ruas jalan Depok-Mergolangu ruas nomor 225, revitalisasi SMPN 1 Watumalang, dan renovasi Gedung Laboratorium Kesehatan Daerah.</p>\r\n\r\n<p style="text-align: justify;">Dipilihnya Pasar Sapuran sebagai lokasi peresmian proyek tahun 2022, kata Zulfa, sebagai upaya Pemkab untuk membangkitkan kembali roda perekonomian masyarakat setelah 2 tahun dilanda badai pandemi covid-19.&nbsp;</p>\r\n\r\n<p style="text-align: justify;">Sementara itu, Bupati Wonosobo Afif Nurhidayat dalam arahannya menuturkan, hasil kegiatan pembangunan 2022 diharapkan bisa dimanfaatkan sebaik-baiknya oleh&nbsp; seluruh lapisan masyarakat Wonosobo, dalam upaya meningkatnya kesehatan, SDM, ekonomi, dan kesejahteraan masyarakat.</p>\r\n\r\n<p style="text-align: justify;">Selain itu ujar Afif, Pemkab Wonosobo berusaha menyajikan kinerja dan tata kelola pemerintahan berbasis&nbsp; asas akuntabilitas dan transparansi. Harapannya masyarakat dapat mengakses secara langsung bagaimana implementasi seluruh kegiatannya di lapangan.</p>\r\n\r\n<p style="text-align: justify;">&ldquo;Berkat kerjasama dari seluruh masyarakat semua proyek strategis berhasil dibangun, semoga dapat termanfaatkan dengan maksimal oleh seluruh elemen masyarakat Wonosobo, selain itu kami&nbsp; juga terbuka menerima catatan dan kritik yang membangun,&rdquo; tandasnya.</p>\r\n\r\n<p style="text-align: justify;">Dalam acara yang dihadiri&nbsp; Forkompimda, Pimpinan DPRD beserta anggota, Pimpinan OPD, Pimpinan BUMN dan BUMD, Camat, Lurah dan Kepala Desa se-Kecamatan Sapuran dan Kepil, serta Perwakilan Paguyuban Pedagang Pasar Sapuran tersebut, juga dilakukan pemberian penghargaan bagi pelaksana terbaik proyek strategis di Wonosobo, kepada, CV Mitra Usaha Mandiri, CV Makmur Langgeng Jaya, dan CV Sendiko Lancar Langgeng.&nbsp;</p>\r\n\r\n<p style="text-align: justify;">Selain itu juga diberikan kepada CV Sakti Media Rekatama, CV Tiga Bersaudara, CV Bagamaya Chakti, CV Rekayasa, dan CV Singgih Pratama Jaya.</p>',
        kata_kunci:
          'Realisasi Belanja Proyek Strategis  Pemkab Wonosobo  Tahun 2022 Capai 89,60%',
        id_kategori: '1',
        informasi_st: 'INFORMASI_ST_02',
        temp: null,
        created_by: 1008,
        created_at: '2023-01-25T01:24:03.000000Z',
        updated_at: '2023-01-26T03:29:04.000000Z',
        updated_by: null,
        deleted_by: null,
        deleted_at: null,
        keterangan:
          'Realisasi belanja Pemerintah Daerah Kabupaten Wonosobo di tahun 2022, capai Rp 1,9 triliun dari pagu anggaran Rp 2,1 triliun atau 89,60%, dalam bentuk belanja...',
        views: 5,
        image: images.foto_4,
        attachment: [
          {
            id_attachment: 24117,
            id_tabel: 1044507,
            path: 'uploads/2023/Januari/',
            file_name: '20230125082403-1_1008.jpg',
            temp: null,
            created_at: '2023-01-25T01:24:03.000000Z',
            updated_at: '2023-01-25T01:24:03.000000Z',
            deleted_at: null,
          },
          {
            id_attachment: 24118,
            id_tabel: 1044507,
            path: 'uploads/2023/Januari/',
            file_name: '20230125082403-2_1008.jpg',
            temp: null,
            created_at: '2023-01-25T01:24:03.000000Z',
            updated_at: '2023-01-25T01:24:03.000000Z',
            deleted_at: null,
          },
          {
            id_attachment: 24119,
            id_tabel: 1044507,
            path: 'uploads/2023/Januari/',
            file_name: '20230125082403-3_1008.jpg',
            temp: null,
            created_at: '2023-01-25T01:24:03.000000Z',
            updated_at: '2023-01-25T01:24:03.000000Z',
            deleted_at: null,
          },
          {
            id_attachment: 24120,
            id_tabel: 1044507,
            path: 'uploads/2023/Januari/',
            file_name: '20230125082403-4_1008.jpg',
            temp: null,
            created_at: '2023-01-25T01:24:03.000000Z',
            updated_at: '2023-01-25T01:24:03.000000Z',
            deleted_at: null,
          },
        ],
        gambar_muka: {
          id_attachment: 24117,
          id_tabel: 1044507,
          path: 'uploads/2023/Januari/',
          file_name: '20230125082403-1_1008.jpg',
          temp: null,
          created_at: '2023-01-25T01:24:03.000000Z',
          updated_at: '2023-01-25T01:24:03.000000Z',
          deleted_at: null,
        },
      },
      {
        id_posting: 1044505,
        posisi: 'highlight',
        judul_posting:
          'PPID Pelaksana Wonosobo Diminta Bantu Pertahankan Predikat Keterbukaan Informasi : Informatif',
        slug: 'ppid-pelaksana-wonosobo-diminta-bantu-pertahankan-predikat-keterbukaan-informasi-informatif',
        isi_posting:
          '<p style="text-align: justify;">Pemerintah Kabupaten Wonosobo membuktikan komitmennya&nbsp;dalam mewujudkan&nbsp;keterbukaan informasi publik. Yang ditandai dengan apresiasi dari Komisi Informasi Publik Provinsi Jawa Tengah,&nbsp;bahwa penyelenggaraan pemerintahan Kabupaten Wonosobo dari menuju informatif menjadi informatif. Hal ini, tentu&nbsp;menjadi parameter awal&nbsp;dalam&nbsp;merancang strategi&nbsp;untuk&nbsp;mempertahankan&nbsp;dan meningkatkan atas capaian yang sudah diraih. Sebagaimana disampaikan Kepala Dinas Komunikasi dan Informatika Kabupaten Wonosobo Fahmi Hidayat dalam arahannya saat Rapat Koordinasi PPID Pelaksana Kabupaten Wonosobo Tahun 2023, di Ruang Rapat Utama Diskominfo, Rabu, (18/01/2023).</p>\r\n\r\n<p style="text-align: justify;">Lebih lanjut Fahmi menyampaikan, dampak dari level informatif sangatlah&nbsp;besar bagi dunia usaha, investor, dan lembaga pers. Untuk itu,&nbsp;PPID Pelaksana Wonosobo diminta&nbsp;memaksimalkan kualitasnya pada&nbsp;pengelolaan&nbsp;website dan media sosial,&nbsp;mengingat keterbukaan merupakan salah satu mandat reformasi 1999&nbsp;yang harus bisa direalisasikan.</p>\r\n\r\n<p style="text-align: justify;">Menurut&nbsp;Fahmi, teknologi digital&nbsp;akan berdampak&nbsp;efektif&nbsp;ketika menerapkan&nbsp;strategi komunikasi yang baik, begitu juga sebaliknya. Maka&nbsp;dalam melaksanakan aktivitas&nbsp;di pemerintahan, kita&nbsp;dituntut&nbsp;lebih informatif melalui pemanfaatan teknologi digitalisasi&nbsp;yang ada saat&nbsp;ini.</p>\r\n\r\n<p style="text-align: justify;">&ldquo;Strategi komunikasi digital kita harus lebih optimal lagi, bagaimana memudahkan seluruh masyarakat dalam mengakses layanan sosial dan layanan publik lainnya,&nbsp;mari bersama kita desain inovasi publik berbasis digital&nbsp;dengan&nbsp;lebih baik,&nbsp;kurang lebih 13 bulan ke depan menuju Pemilu 2024, PPID Pelaksana diminta menyikapi secara bijak karena tingginya tensi pemilu yang akan dihadapi,&rdquo;&nbsp;pinta Fahmi.</p>\r\n\r\n<p style="text-align: justify;">Kepala Bidang Informatika Diskominfo Wonosobo Sugeng Riyadi menambahkan, capaian SPBE Pemkab Wonosobo di tahun 2022 masuk kategori baik meskipun capaian indeks angkanya masuk kategori sedang, karena terdapat indikator penilaian tambahan yang belum bisa dipenuhi sehingga mengurangi bobot penilaian.</p>\r\n\r\n<p style="text-align: justify;">&ldquo;Tahun 2023 ini Diskominfo akan membenahi tata kelola data di Perangkat Daerah sebelum dipublikasikan lewat Satu Data, untuk itu dibutuhkan keterlibatan semua sumber daya yang mumpuni dan memahami betul ketersediaan keseluruhan data di PPID Pelaksana,&rdquo; pungkas Sugeng.</p>\r\n\r\n<p style="text-align: justify;">Sementara itu, Kepala Bidang Informasi Komunikasi Publik Diskominfo Wonosobo Aldhiana Kusumawati S.STP menambahkan, pihaknya selama kurang lebih 2 bulan ke depan akan fokus membenahi perbaikan layanan informasi, penyusunan daftar informasi publik, perbaikan kelembagaan PPID Pembantu, dan pemutakhiran informasi berkala di media sosial.</p>',
        kata_kunci:
          'PPID Pelaksana Wonosobo Diminta Bantu Pertahankan Predikat Keterbukaan Informasi : Informatif',
        id_kategori: '1',
        informasi_st: 'INFORMASI_ST_02',
        temp: null,
        created_by: 1008,
        created_at: '2023-01-19T07:50:19.000000Z',
        updated_at: '2023-01-26T03:30:10.000000Z',
        updated_by: null,
        deleted_by: null,
        deleted_at: null,
        keterangan:
          'Pemerintah Kabupaten Wonosobo membuktikan komitmennya dalam mewujudkan keterbukaan informasi publik. Yang ditandai dengan apresiasi dari Komisi Informasi...',
        views: 41,
        image: images.foto_5,
        attachment: [
          {
            id_attachment: 24108,
            id_tabel: 1044505,
            path: 'uploads/2023/Januari/',
            file_name: '20230119025019-1_1008.jpg',
            temp: null,
            created_at: '2023-01-19T07:50:19.000000Z',
            updated_at: '2023-01-19T07:50:19.000000Z',
            deleted_at: null,
          },
          {
            id_attachment: 24109,
            id_tabel: 1044505,
            path: 'uploads/2023/Januari/',
            file_name: '20230119025019-2_1008.jpg',
            temp: null,
            created_at: '2023-01-19T07:50:19.000000Z',
            updated_at: '2023-01-19T07:50:19.000000Z',
            deleted_at: null,
          },
          {
            id_attachment: 24110,
            id_tabel: 1044505,
            path: 'uploads/2023/Januari/',
            file_name: '20230119025019-3_1008.jpg',
            temp: null,
            created_at: '2023-01-19T07:50:19.000000Z',
            updated_at: '2023-01-19T07:50:19.000000Z',
            deleted_at: null,
          },
          {
            id_attachment: 24111,
            id_tabel: 1044505,
            path: 'uploads/2023/Januari/',
            file_name: '20230119025019-4_1008.jpg',
            temp: null,
            created_at: '2023-01-19T07:50:19.000000Z',
            updated_at: '2023-01-19T07:50:19.000000Z',
            deleted_at: null,
          },
          {
            id_attachment: 24112,
            id_tabel: 1044505,
            path: 'uploads/2023/Januari/',
            file_name: '20230119025019-5_1008.jpg',
            temp: null,
            created_at: '2023-01-19T07:50:19.000000Z',
            updated_at: '2023-01-19T07:50:19.000000Z',
            deleted_at: null,
          },
        ],
        gambar_muka: {
          id_attachment: 24108,
          id_tabel: 1044505,
          path: 'uploads/2023/Januari/',
          file_name: '20230119025019-1_1008.jpg',
          temp: null,
          created_at: '2023-01-19T07:50:19.000000Z',
          updated_at: '2023-01-19T07:50:19.000000Z',
          deleted_at: null,
        },
      },
    ],
  };
  return (
    <View style={styles.container}>
      <Header headerTitle="List News" />
      <View>
        <FlatList
          data={data.data}
          horizontal
          showsHorizontalScrollIndicator={false}
          onEndReachedThreshold={0.1}
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('Detail', {
                  params: {data: item},
                })
              }
              style={styles.newsCard}>
              <View>
                <Image
                  source={item.image}
                  style={styles.newsThumbnail}
                  resizeMode="contain"
                />
                <View>
                  <Text style={styles.newsTitle} numberOfLines={3}>
                    {item?.judul_posting}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
      <View style={styles.newsToday}>
        <Text style={styles.newsTitle}>Today's News</Text>
      </View>
      <FlatList
        data={data.data}
        showsVerticalScrollIndicator={false}
        onEndReachedThreshold={0.1}
        renderItem={({item}) => (
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('Detail', {
                params: {data: item},
              })
            }>
            <View style={styles.contentNews}>
              <View style={{flexDirection: 'row', marginHorizontal: 20}}>
                <Image
                  source={item.image}
                  style={{width: 64, height: 64, borderRadius: 12}}
                />
                <View
                  style={{
                    marginLeft: 13,
                    flex: 2,
                    justifyContent: 'space-between',
                  }}>
                  <Text style={styles.titleNews} numberOfLines={2}>
                    {item?.judul_posting}
                  </Text>
                  <Text style={styles.titleDate}>
                    {moment(item?.created_at).format(
                      'dddd, DD MMMM YYYY HH:mm',
                    )}
                  </Text>
                </View>
              </View>
              <View style={styles.divider} />
            </View>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  newsCard: {
    backgroundColor: 'white',
    margin: 10,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    // shadow
    shadowColor: 'rgba(46, 50, 132, 0.15)',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 4,
    width: 185,
    height: 250,
  },
  newsThumbnail: {
    width: 165,
    height: 165,
    borderRadius: 10,
  },
  newsTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000000',
  },
  newsToday: {
    margin: 16,
  },
  row: {
    flexDirection: 'row',
  },
  newsCardRow: {
    backgroundColor: 'white',
    margin: 10,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    // shadow
    shadowColor: 'rgba(46, 50, 132, 0.15)',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 4,
  },
  newsThumbnailRow: {
    width: 75,
    height: 75,
    borderRadius: 10,
  },
  contentNews: {
    marginTop: 20,
  },
  titleNews: {
    color: '#000000',
    fontWeight: 'bold',
  },
  titleDate: {
    color: '#75778E',
    fontSize: 12,
  },
  divider: {
    height: 1,
    marginTop: 16,
    marginHorizontal: 20,
    backgroundColor: '#F1F1F1',
    width: '90%',
  },
});
